
CREATE ROLE testador WITH LOGIN PASSWORD 'testador';
CREATE DATABASE tcc WITH OWNER testador;

\c tcc

CREATE TABLE equipment (
	id SERIAL PRIMARY KEY,
	name TEXT NOT NULL,
	acronym TEXT,
	whatis TEXT NOT NULL,
	function TEXT NOT NULL,
	howtouse TEXT NOT NULL,
	curiosities TEXT
);

INSERT INTO equipment(name, acronym, whatis, function, howtouse, curiosities) VALUES(
	'Bateria Selada de Chumbo Ácido',
	'SLA (Sealed Lead Acid – Bateria Selada de Chumbo Ácido). VRLA (Valve Regulated Lead Acid – Bateria de Chumbo Ácido Regulada por Válvula).',
	'É a bateria mais econômica quando o problema do peso pode ser desprezado.',
	'SLA - É bastante usada em equipamentos hospitalares, cadeira de rodas elétricas, luz de emergência, automóveis, empilhadeiras e grandes sistemas de fornecimento de energia elétrica ininterrupta (nobreaks). VRLA – Usos típicos são em repetidoras telefônicas, centros de distribuição de energia, hospitais, bancos, aeroportos e instalações militares.',
	'Ao contrário das baterias de chumbo-ácido com eletrólito líquido, ambas as baterias SLA e VRLA são projetadas para uma sobre tensão baixa, de forma a evitar a formação de gases durante a carga. Carga em excesso pode causar aparecimento de gás e depleção de água. Consequentemente, as baterias SLA e VRLA não podem nunca ser recarregadas em todo seu potencial. As baterias SLA devem sempre ser armazenadas carregadas. Deixar a bateria descarregada causa sulfatação, uma condição que torna difícil, se não impossível, recarregar as baterias.',
	'Inventadas em 1859 pelo físico francês Gaston Planté, as baterias de chumbo ácido foram as primeiras baterias para uso comercial. Engenheiros podem argumentar que a palavra “bateria selada” é um engano já que nenhuma bateria pode ser totalmente selada. Em essência, todas são reguladas com válvulas. SLA - 0,2Ah até 30Ah. VRLA – 30Ah até Milhares de Ah.'
);

INSERT INTO equipment(name, acronym, whatis, function, howtouse, curiosities) VALUES(
	'Fonte de Alimentação Ininterrupta',
	'UPS (Uninterruptible Power Supply – Fonte de Energia Ininterrupta). No-break (no Brasil).',
	'É um sistema secundário de energia elétrica que entra em ação, alimentando os dispositivos a ele ligados, quando há interrupção no fornecimento de energia primária.',
	'O no-break, além de proteger os aparelhos em casos de quedas de energia, serve para conter subtensão ou sobretensão na rede elétrica, sobrecarga, descarga das baterias, curto circuito nas saídas, picos de tensão e fornece uma energia limpa e ininterrupta.',
	'É possível ligar o nobreak em uma régua ou filtro de linha. A recomendação é fazer a ligação do dispositivo ao filtro de linha, pois esse equipamento tem mais capacidade de protegê-lo. De acordo com especialistas, não é recomendado instalar estabilizadores ao nobreak por questões de segurança. Um dos fatores que explicam esse procedimento é que o nobreak necessita recarregar a bateria depois da utilização. Isso faz com que o consumo em volt-ampère (VA) aumente em 15% ou até mais. Caso o estabilizador tenha capacidade igual ou menor do que o nobreak, ele poderá desligar ou explodir ao excedê-la.',
	'É importante salientar que existem dois tipos de nobreaks: online e offline. O modelo online faz o chaveamento para o uso da bateria no momento em que a energia é cortada. Já o offline demora uma fração de segundo para ativar a bateria. De modo geral, por ser mais caro, o primeiro é indicado principalmente para de quem tem um servidor ou um equipamento muito sensível. Entretanto, qualquer nobreak é maior e mais pesado do que simples estabilizadores. Os nobreaks, de acordo com especialistas, resolvem o problema da instabilidade da rede elétrica de maneira mais eficiente. Os modelos de nobreaks podem ser online e offline. Os online são indicados para instalações comerciais, industriais e escritórios; enquanto os offline são opções para uma residência. Os mais simples custam, em média, a partir de R$ 170 reais. No entanto, aconselha-se examinar as características dos equipamentos que deseja conectar a ele para efetuar uma compra adequada. E lembre-se, este aparelho serve para que o usuário não perca dados importantes quando houver uma queda de energia. Não pense em comprá-lo para continuar trabalhando por horas sem energia elétrica.'
);

INSERT INTO equipment(name, whatis, function, howtouse, curiosities) VALUES(
    'Estabilizador',
    'O estabilizador é o equipamento utilizado, normalmente, para ligar computadores desktops e seus periféricos, como impressoras, monitores, alguns modelos de caixas de som etc.',
    'A função deste dispositivo, como o próprio nome sugere, é estabilizar a tensão elétrica de entrada, de forma que a saída forneça sempre a mesma tensão.',
    'Primeiro, antes de ligar, é importante verificar a potência do aparelho. Se não for bivolt, observe se a tensão da sua rede elétrica é compatível com a tensão da entrada de seu estabilizador. Verifique se a tensão de saída do seu estabilizador é compatível com a tensão de entrada de seus equipamentos. Nunca instale o estabilizador em locais úmidos, procure evitar lugares que contenham líquidos inflamáveis e vapores químicos. Deixe um espaço livre para a ventilação de no mínimo 2,5 cm (1 pol.), para não bloquear as saídas de ventilação laterais ou em cima do aparelho. Faça a ligação entre a entrada do estabilizador e a rede local usando condutores compatíveis. Para não danificar o aparelho, limpar somente com pano umedecido em álcool. Não deve ser lavado ou molhado com água ou outro solvente. Atenção: refrigeradores, freezers, ferramentas elétricas, aparelhos de ar condicionado, desumidificadores, liquidificadores ou quaisquer aparelhos operados por um motor CA não são compatíveis com estabilizadores. Pode ser usado também como extensores de capacidade para tomadas, substituindo os “benjamins”, por apresentarem maior segurança, pois fornecem energia para vários aparelhos ao mesmo tempo, sem risco de curto-circuito.',
    'A faixa de preço de um bom modelo de 300 VA é de R$ 50 a R$ 100. Caso seja necessário conectar equipamentos que precisem de mais corrente para funcionar ou mais aparelhos em um único estabilizador, aconselha-se a utilização de um com, pelo menos, 600 VA, que custa, em média, a partir de R$ 180.'
);

INSERT INTO equipment(name, acronym, whatis, function, howtouse, curiosities) VALUES(
    'Filtro de Linha',
    'Régua',
    'São dispositivos equipados com um fusível, varistores, capacitores e indutores.',
    'O objetivo deste equipamento é evitar a passagem de altas correntes para os aparelhos nele conectados. Quando isso ocorre, o fusível “queima”, ou seja, corta a energia que alimenta o filtro. Os varistores, em combinação com capacitores e indutores, controlam a entrada de longos picos de voltagem, além de garantir filtragem contra altas frequências, produzidas por equipamentos como liquidificadores, batedeiras, alguns ventiladores, entre outros.',
    'Conectar o filtro de linha na tomada e depois ligar eletrônicos em suas entradas.',
    'Existem diversos tipos de filtros de linha no mercado. Alguns modelos, inclusive, tentam enganar o consumidor, por não conter tais componentes eletrônicos, servindo somente como um multiplicador de tomadas. Por este motivo, é importante observar atentamente as características do produto. Procure por descrições como “Protetor contra surtos”, incluindo características de cuidados contra curto-circuito, sobrecargas e descargas elétricas. Além disso, o selo do Inmetro é indispensável. Os preços dos filtros de linha variam bastante, dependendo da quantidade de tomadas, características e qualidade do material. É possível encontrar modelos de boa qualidade por preços na faixa de R$ 25 a R$ 60.'
);

INSERT INTO equipment(name, whatis, function, howtouse) VALUES(
    'Extensão Elétrica',
    'É um extensor de tomada.',
    'Serve para ampliar o alcance da tomada de energia, tornando possível conectar aparelhos elétricos distantes.',
    'Conectar a extensão na tomada e depois conectar os aparelhos nas tomadas da extensão ou vice-versa.'
);

INSERT INTO equipment(name, whatis, function, howtouse) VALUES(
    'Cartucho de Toner',
    'É um recipiente para tinta em pó.',
    'O toner é colorido e origina impressões de máquinas laser, pois quando o pó é aquecido, transfere através dos cilindros a imagem a ser gravada para o papel.',
    'O cartucho de toner utiliza a tinta em pó, ou seja, o cartucho de toner é carregado em suas cores primárias, sendo conhecidas como CMYK: Ciano (C), Magenta (M), Amarelo (Y) e Preto (K).'
);

INSERT INTO equipment(name, whatis, function, howtouse) VALUES(
    'Cartucho de Tinta',
    'O cartucho de tinta é um recipiente para tinta líquida.',
    'São utilizados em impressoras jato de tinta, em suma mais caseiras e tem durabilidade inferior ao toner, mesmo levando em conta o conteúdo impresso, imagem, chapado, texto, dentre outros dados a serem impressos.',
    'O cartucho de tinta também vem nas cores primárias (CMYK) - evitando o contato direto com o mecanismo e liberando apenas a quantidade certa no papel.'
);

INSERT INTO equipment(name, whatis, function, howtouse) VALUES(
    'Teclado com Encaixe USB.',
    'Teclado com encaixe retangular.',
    'Utilizado para digitar caracteres na tela do computador.',
    'Encaixar na entrada USB do computador.'
);

INSERT INTO equipment(name, whatis, function, howtouse) VALUES(
    'Teclado com Encaixe PS/2.',
    'Teclado com encaixe redondo.',
    'Utilizado para digitar caracteres na tela do computador.',
    'Desligar o computador, encaixar na entrada PS/2 do computador e ligar o computador.'
);

INSERT INTO equipment(name, acronym, whatis, function, howtouse) VALUES(
    'Porta de Exibição',
    'DisplayPort',
    'O DisplayPort foi lançado em 2006 pela VESA (Video Electronics Standards Association) e é uma interface de vídeo desenvolvida para ser um padrão aberto sem a necessidade que um desenvolvedor qualquer pague royalties para colocar a interface em seu produto.',
    'Fornecer uma interface de vídeo entre o monitor e o computador.',
    'Encaixar na entrada DisplayPort do computador.'
);

INSERT INTO equipment(name, acronym, whatis, function, howtouse, curiosities) VALUES(
    'Interface Multimídia de Alta Resolução',
    'HDMI (High-Definition Multimedia Interface)',
    'É uma interface condutiva digital de áudio e vídeo, capaz de transmitir dados não comprimidos, sendo uma alternativa melhorada aos padrões analógicos, como rádio frequência, VGA e outros.',
    'O HDMI fornece uma interface de comunicação entre qualquer fonte de áudio e vídeo digital, como o Blu-ray, o leitor de DVD, o computador, etc.',
    'Encaixar na entrada HDMI do computador.',
    'Existem dois principais modelos de conectores HDMI no mercado: o tipo A e o tipo B. O cabo HDMI A (com 19 pinos) é o mais popular, pois tem compatibilidade direta com a tecnologia DVI. Enquanto isso, o HDMI B se sobressai por permitir transmissões de altíssima qualidade, trabalhando a partir do sistema dual link.'
);

INSERT INTO equipment(name, acronym, whatis, function, howtouse, curiosities) VALUES(
    'Matriz de Gráficos de Vídeo',
    'VGA (Video Graphics Array). Conector D-SUB. Conector DB.',
    'VGA é um padrão de vídeo criado para os gráficos dos computadores. Esse padrão permite que imagens sejam transmitidas em tempo real de um computador para um monitor que tenha essa saída.',
    'Fornecer uma interface de vídeo entre o monitor e o computador.',
    'Encaixar na entrada VGA do computador.',
    'A conexão VGA é analógica e não transmite áudio.'
);

INSERT INTO equipment(name, acronym, whatis, function, howtouse, curiosities) VALUES(
    'Porta Universal',
    'USB (Universal Serial Bus)',
    'É um tipo de tecnologia que permite a conexão de periféricos sem a necessidade de desligar o computador, além de transmitir e armazenar dados.',
    'Transmissão de dados entre dispositivos operacionais.',
    'Encaixar na entrada USB do computador.',
    'As atuais conexões USB são do padrão PnP (Plug and Play ou “Plugar e Usar”, em português), que ajudou a diminuir toda a complicação existente na configuração desses dispositivos, com o objetivo de facilitar a vida do usuário.'
);

INSERT INTO equipment(name, acronym, whatis, function, howtouse, curiosities) VALUES(
    'Interface Visual Digital',
    'DVI-D (Digital Visual Interface-Digital). DVI-A (Digital Visual Interface-Analog). DVI-I (Digital Visual Interface-Integrated).',
    'É um conector que usa sinal digital para transferir imagens da placa de vídeo para monitores e projetores digitais, como o display LCD.',
    'Fornecer uma interface de vídeo entre o monitor e o computador.',
    'Encaixar na entrada DVI do computador.',
    'O cabo DVI só transmite vídeo.'
);

GRANT SELECT ON equipment TO testador;
