# Especifica a imagem pai.
FROM debian:stretch

# Configura o diretório de trabalho para /TCC-DATABASE.
# Todos os comandos subsequentes serão efetuados neste diretório.
WORKDIR /TCC-DATABASE

# Copia o conteúdo do diretório atual /TCC-DATABASE para dentro do container
# em um diretório com o mesmo nome /TCC-DATABASE.
COPY . /TCC-DATABASE

# Instalação e configuração do PostgreSQL.
RUN apt update \
	&& apt install --yes wget \
	&& apt install --yes gnupg \
	&& wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
	&& apt install --yes software-properties-common \
	&& add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" \
	&& apt update \
	&& apt install --yes postgresql-11 \
	&& cat autenticar | passwd postgres \
	&& chown postgres:ssl-cert /etc/ssl/private \
	&& chown postgres:ssl-cert /etc/ssl/private/ssl-cert-snakeoil.key \
	&& chmod 500 /etc/ssl/private \
	&& chmod 500 /etc/ssl/private/ssl-cert-snakeoil.key \
	&& sed -i "59c\listen_addresses = '*'" /etc/postgresql/11/main/postgresql.conf \
	&& sed -i "92c\host all all 0.0.0.0/0 md5" /etc/postgresql/11/main/pg_hba.conf

# Configura variáveis de ambiente para execução do servidor postgres.
ENV PATH=$PATH:/usr/lib/postgresql/11/bin
ENV PGDATA=/etc/postgresql/11/main

# Expõem a porta 5432 no mundo do lado de fora deste container.
EXPOSE 5432

# Configura o usuário que irá executar a imagem.
USER postgres:postgres

# População do SGBD.
RUN pg_ctl start \
	&& psql --file=/TCC-DATABASE/database.sql \
	&& psql --command='SELECT name FROM equipment;' tcc postgres

# Executa o servidor de banco de dados quando o container é executado.
CMD ["postgres"]
